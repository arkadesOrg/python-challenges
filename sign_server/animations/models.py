from django.db import models

class Animation(models.Model):
  name = models.CharField(max_length=255,
                          blank=False,
                          default="animation")
  filepath = models.OneToOneField('items.Item',
                              on_delete=models.CASCADE,
                              primary_key=True,
                              blank=False)
  
  
class AnimationSequence(models.Model):
  animation = models.ForeignKey(Animation,
                              on_delete=models.CASCADE,
                              blank=False,
                              default=1,)
  sequence = models.IntegerField()
  
  class Meta:
    unique_together: ('animation', 'sequence')
    ordering: ['animation', 'sequence']
    
  def __str__(self):
    return "Sequence Element %s: %s" % (self.sequence, self.animation.name)
  
