from animations.models import Animation, AnimationSequence
from items.models import Item
from rest_framework import serializers


class AnimationSerializer(serializers.ModelSerializer):
  filepath = serializers.CharField(max_length=255)
  
  class Meta:
    model = Animation
    fields = ('name', 'filepath')
  
  def create(self, validated_data):
    item, created = Item.objects.get_or_create(content=validated_data['filepath'])
    item.save()
    animation = Animation.objects.get_or_create(name=validated_data['name'], filepath=item)
    return validated_data
  
class AnimationSequenceSerializer(serializers.ModelSerializer):
  class Meta:
    model = AnimationSequence
    fields = ('animation', 'sequence')
    
 
