from sign_server.routers import SignRouter
from . import views

router = SignRouter()
router.register(r'', views.AnimationViewSet)

urlpatterns = router.urls
