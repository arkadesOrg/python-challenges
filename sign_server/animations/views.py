from rest_framework import viewsets
from animations.models import Animation
from animations.serializers import AnimationSerializer


class AnimationViewSet(viewsets.ModelViewSet):
  """
  API endpoint that allows animations to be viewed.
  """
  http_method_names = ['get', 'post', 'delete']
  queryset = Animation.objects.all()
  serializer_class = AnimationSerializer
    

