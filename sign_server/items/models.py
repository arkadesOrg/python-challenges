from django.db import models


class Item(models.Model):
  content = models.CharField(max_length=255,
                          blank=True,
                          default="/path/to/animation/item",
                          unique=True)
  
  def __str__(self):
    return "%s" % (self.content)
