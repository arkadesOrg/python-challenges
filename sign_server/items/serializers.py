from items.models import Item
from rest_framework import serializers


class ItemSerializer(serializers.ModelSerializer):
  content = serializers.CharField()
  
  class Meta:
    model = Item
    fields = ('content',)
