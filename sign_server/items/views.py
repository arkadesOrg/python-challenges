from django.shortcuts import render
from rest_framework import viewsets
from items.serializers import ItemSerializer
from items.models import Item


class ItemViewSet(viewsets.ModelViewSet):
  """
  API endpoint that allows items to be viewed.
  """
  http_method_names = ['get']
  queryset = Item.objects.all()
  serializer_class = ItemSerializer
