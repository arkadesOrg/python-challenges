"""sign_server URL Configuration
"""
#from django.contrib import admin
from django.urls import include, path

urlpatterns = [
  path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
  path('items/', include('items.urls')),
  path('animations/', include('animations.urls')),
  path('synonyms/', include('synonyms.urls')),
  #path('admin/', admin.site.urls),
]
