import re
from synonyms.apps import logger


class Lookup(object):
  sequencer = None
  prepend = False
  value = ''
  
  def __init__(self, value, sequencer=None, prepend=False):
    self.prepend=prepend
    self.value = value
    self.sequencer = sequencer
  
  def found(self, result):
    self.sequencer.found(result)
    
  def __str__(self):
    return self.value
  
  
class LookupSequencer(object):
  text = ''
  reference = None
  current = 0
  balance = 0
  complementary = 0
  words = []
  first = True
  
  def __init__(self, text):
    self.reference = text
    self.text = str(text)
    self.words = list(filter(None, self.text.split(' ')))
    
  def is_balanced(self):
    if self.current == self.complementary:
      return True
    return False
    
  def get_iteration(self):
    return max(self.current, self.complementary)
    
  def gen_lookups(self):
    result = []
    iteration = self.get_iteration()
    wordcount = len(self.worklist)
    if iteration > wordcount:
      iteration = wordcount
    
    if self.is_balanced():
      self.current += 1
      text = ' '.join(self.worklist[iteration:wordcount])
      return Lookup(text, sequencer=self)
    else:
      self.complementary += 1
      text = ' '.join(self.worklist[0:wordcount - iteration])
      return Lookup(text, sequencer=self, prepend=True)
    
    
  
  def found(self, result):
    if self.is_balanced():
      self.balance += 1
      self.reference.lookup_results.insert(self.balance-1, result)
      
      self.worklist = self.worklist[len(self.worklist)-self.current:len(self.worklist)]
    else:
      self.reference.lookup_results.insert(self.balance, result)
      self.worklist = self.worklist[0:self.complementary]
    
    self.current = 0
    self.complementary = 0
    if not self.worklist:
      if hasattr(self.reference, 'lookup_complete'):
        self.reference.lookup_complete = True
    
    
  def __iter__(self):
    return self

  def __next__(self):
    if self.current == 0 and self.first:
      self.worklist = self.words
      self.first = False
    
    if self.complementary > len(self.worklist) \
      or self.current > len(self.worklist):
      self.worklist = self.worklist[1:len(self.worklist)-1]
      logger.info('reset worklist: %s' % self.worklist)
      self.current = 0
      self.complementary = 0
      
    if len(self.worklist) == 0:
      raise StopIteration
    else:
      return self.gen_lookups()
    


class Sentence(object):
  content = ''
  lookup_complete = False
  lookup_results = None
  type = 'statement'
  charfilter = re.compile(r'[^ A-Za-zäüöß0-9]+', re.UNICODE)
  
  def __init__(self, content, type):
    self.lookup_results = []
    self.type = type
    self.content = self.charfilter.sub(r'', content, re.UNICODE).strip()
    
  def __str__(self):
    return '%s' % self.content
  
  def __repr__(self):
    return 'Senctence: \'%s\'' % self.content

class SentenceSequencer(object):
  text=''
  delimiters = {
    '.': 'statement',
    '?': 'question',
    '!': 'exclamation'
      }
  subordinate = ','
  sentences = []
  current = 0
  
  def __init__(self, text, *args, **kwargs):
    self.text = str(text)
    self.sentences = self.gen_sentences(text)

    
  def gen_sentences(self, text):
    sentences = []
    all_delimiters = ''.join(self.delimiters.keys())
    
    delimiter_regex = re.compile(f'[^{all_delimiters}]+', re.UNICODE)
    
    delimiter_list = list(delimiter_regex.sub(r'', text, re.UNICODE))
    
    if not delimiter_list:
      sentences = [Sentence(text, self.delimiters['.'])]
    else:
      for d in delimiter_list:
        sentence, text = tuple(text.split(d, 1))
        if sentence:
          sentences.append(Sentence(sentence, self.delimiters[d]))
        
    return sentences
    
    
  def __iter__(self):
    return self
    
  def __next__(self):
    if self.current >= len(self.sentences):
      raise StopIteration
    else:
      self.current += 1
      return self.sentences[self.current - 1]
      
