from django.apps import AppConfig

import logging
logger = logging.getLogger('django')
logger.setLevel(logging.INFO)

class SynonymsConfig(AppConfig):
    name = 'synonyms'
