from django.db import models


class Synonym(models.Model):
  name = models.CharField(max_length=255,
                          blank=False,
                          unique=True,
                          default="Synonym")
  
  animations = models.ManyToManyField('animations.AnimationSequence',
                                    blank=False)
  def __str__(self):
    return "%s: %s" % (self.pk, self.name)
