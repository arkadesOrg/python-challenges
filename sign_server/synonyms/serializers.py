from django.utils import six
from synonyms.apps import logger
from animations.models import Animation, AnimationSequence
from animations.serializers import AnimationSerializer
from items.models import Item
from synonyms.models import Synonym
from django.db.models.fields import related_descriptors

from rest_framework import serializers


  
class AnimationChoiceField(serializers.MultipleChoiceField):
  def __init__(self, *args, **kwargs):
    
    try:
      names = Animation.objects.all().values_list('name', flat=True)
      pks = Animation.objects.all().values_list('pk', flat=True)
      choices = tuple(zip(pks, names))
    except:
      choices = ()
    super(AnimationChoiceField, self).__init__(choices=choices)
  
  
  def to_representation(self, value):
    try:
      value = value.get_queryset()
      value = [item.pk for item in value]
    except:
      pass
    
    return super(AnimationChoiceField, self).to_representation(value)
  
  def update(self):
    self.__init__()
    
  
    
class SynonymSerializer(serializers.ModelSerializer):

  class Meta:
    model = Synonym
    fields = ('name', 'animations')
  
  animations = AnimationChoiceField ()
  
  def create(self, validated_data):
    try:
      synonym = Synonym.objects.get(name=validated_data['name'])
    except:
      synonym = Synonym.objects.create(name=validated_data['name'])
    #synonym, created = Synonym.objects.get_or_create(name=validated_data['name'])
    
    pks = validated_data['animations']
    
    animations = Animation.objects.filter(pk__in=pks)
    for i, a in enumerate(animations):
      animation_seq, created = AnimationSequence.objects.get_or_create(animation=a, sequence=i)
      animation_seq.save()
      synonym.animations.add(animation_seq)
    
    synonym.save()
    return validated_data
    
    
  
  
