import json
import requests

from django.http import HttpRequest
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from synonyms.apps import logger

sentence = "Die Katze läuft zu einem Baum, klettert den Baum hoch und setzt sich in den Baum. Da kommt plötzlich ein Hund und läuft auf den Baum zu. Die Katze läuft schnell den Baum herunter und läuft weg."


class SynonymTests(TestCase):
    def test_get_endpoint(self):
        """
        Query Synonym-Animation pairs from text.
        (not working yet)
        """
        return
        requesturi = reverse('synonyms-list')
        uri = 'http://localhost:8000%s' % requesturi
        data = {'query': sentence}
        
        # client.get doesn't support HTTP GET with body, which is basically non-standard but okay.
        #response = self.client.get(url, data, format='json')
        
        response = requests.get(uri, json=data)
        self.assertEqual(json.loads(response.content), {'id': 4, 'username': 'lauren'})
        
    def test_from_text_post_endpoint(self):
        """
        Query Synonym-Animation pairs from text.
        """
        requesturi = reverse('synonyms-text')
        uri = 'http://localhost:8000%s' % requesturi
        data = {'query': sentence}
        
        
        response = requests.post(uri, json=data)
        logger.info(json.loads(response.content))
        self.assertEqual(json.loads(response.content), {'id': 4, 'username': 'lauren'})
        
