from sign_server.routers import SignRouter
from . import views
from synonyms.apps import logger

router = SignRouter(trailing_slash=False)
router.register(r'', views.SynonymView, base_name='synonyms')
router.register(r'translate', views.SynonymTranslate, base_name='synonyms')

urlpatterns = router.urls


#for url in router.urls:
    #logger.info(url.__dict__)
