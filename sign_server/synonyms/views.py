import json, re
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.decorators.vary import vary_on_cookie

from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import viewsets

from synonyms.serializers import SynonymSerializer
from synonyms.models import Synonym
from synonyms.apps import logger

from speech.sequencer import SentenceSequencer, LookupSequencer


class SynonymView(viewsets.ModelViewSet):
  """
  API endpoint that allows synonyms to be viewed.
  """
  http_method_names = ['get', 'post', 'delete']
  queryset = Synonym.objects.all()
  serializer_class = SynonymSerializer
  
  # Cache url for each synonym for 1 hours
  @method_decorator(cache_page(60*60*1))
  @method_decorator(vary_on_cookie)
  def list(self, request):
    logger.info('request: %s' % request.POST.keys())
    queryset = Synonym.objects.all()
    serializer = SynonymSerializer(queryset, many=True)
    
    return Response(serializer.data)
 
 
class SynonymTranslate(viewsets.ModelViewSet):
  """
  API endpoint that query synonyms by context.
  """
  
  @action(detail=True, methods=['post'])
  @method_decorator(cache_page(60*60*1))
  @method_decorator(vary_on_cookie)
  def text(self, request):
    synonyms = []
    try:
      query = request.data['query']
      sequencer = LookupSequencer(query)
    except:
      raise
    
    for sentence in SentenceSequencer(query):
      logger.info('sentence: %s' % sentence)
      for lookup in LookupSequencer(sentence):
        try:
          synonym = Synonym.objects.get(name__iexact=lookup)
          serializer = SynonymSerializer(synonym)
          lookup.found(
            {synonym.name: list(serializer.data['animations'])}
            )
        except Synonym.DoesNotExist:
          pass
        except:
          raise
      logger.info('result: %s' % str(sentence.lookup_results))
      synonyms.extend(sentence.lookup_results)
    logger.info(synonyms)
    return JsonResponse(synonyms, safe=False)
    
